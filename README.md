Bootstrap alerts for Codeigniter 3
==================
Bootstrap 4 stellt «Alerts» zur Verfügung. Um diese in Codeigniter nicht immer wieder neu zu schreiben erstellte ich mir dieser helper.
# Installation
Die Datei «alert_helper.php» in *application/helper* kopieren
# Verwendung
In den Dateien Demo ist unter *application/view* und *application/controller* ein Beispiel wie die Klasse verwendet wird.

##### Die Helper Klasse laden
Kann auch in der Konfigurationsdatei *application/config/config.php*  permanent geladen werden.
```php
$this->load->helper('alert_helper');
und
$this->load->library('session');
```

##### Controller
Im Controller die Meldung in die Session speichern
```php
$this->session->set_flashdata('alert', success("This is the Bootstrap alert Box message text. Nice one!"));
oder
$this->session->set_flashdata('alert', danger("This is the Bootstrap alert Box message text. Something went wrong!"));
```
##### View
Der *View* den Bootstrap Alert code hinzufügen. Im Controller wird die Meldung in eine Session gespeichert daher muss dies Session Bibliothek auch geladen sein.
```php
<?php $alert =  $this->session->flashdata('alert');
    if($alert){
    ?>
    <div class="alert <?php echo $alert['class'];?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php echo $alert['title'];?></strong> <?php echo $alert['message'];?>
    </div>
    <?php
    }
?>
```