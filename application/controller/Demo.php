<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Demo extends CI_Controller {

    public function __construct(){
        parent::__construct();
		// Load the Codeigniter helper
        $this->load->helper('alert_helper');
    }

    public function index()
    {
		// Do some magic stuff here and get the result
		
		$result = true //or false
		
		if($result){
			$this->session->set_flashdata('alert', success("This is the Bootstrap alert Box message text. Nice one!"));
		} 
		else {
			$this->session->set_flashdata('alert', danger("This is the Bootstrap alert Box message text. Something went wrong!"));
		}
        $this->load->view('demo');
    }
}