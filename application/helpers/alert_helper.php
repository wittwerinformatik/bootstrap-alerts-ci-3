<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This functions helps to handle Bootstrap 4 alerts.
 */

/**
* createAlert
*
* Create the Boostrap Alert.
* 
* @since	version 1.0
* @param	string name of the bootstrap message class
* @param	string message text
* @return	array	depends what the array contains.
*/
function createAlert($type, $message){
    $class;
    $title;
    switch ($type) {
        case 'success':
            $class = "alert-success";
            $title = "Success!";
            break;
        case 'info':
            $class = "alert-info";
            $title = "Info!";
            break;
        case 'warning':
            $class = "alert-warning";
            $title = "Warnung!";
            break;
        case 'danger':
            $class = "alert-danger";
            $title = "Fehler!";
            break;
        case 'primary':
            $class = "alert-primary";
            $title = "Primary!";
            break;
        case 'secondary':
            $class = "alert-secondary";
            $title = "Secondary!";
            break;
        case 'dark':
            $class = "alert-dark";
            $title = "Dark!";
            break;
        case 'light':
            $class = "alert-light";
            $title = "Light!";
            break;
        default:
            $class = "alert-light";
            $title = "Light!";
            break;
    }
    return array(
        'class' => $class, 
        'title' => $title, 
        'message' => $message
    );
}

if ( ! function_exists('success'))
{
    /**
     * success
     *
     * Create a Bootstrap 4 Alert "Success" element with a close button
     *
     * @param	string
     * @return	array	depends on what the array contains
     */
    function success($message = false)
    {
        return createAlert('success', $message);
    }
}
if ( ! function_exists('info'))
{    
    /**
    * info
    *
    * Create a Bootstrap 4 Alert "Info" element with a close button
    *
    * @param	string
    * @return	array	depends on what the array contains
    */
  function info($message = false)
  {
    return createAlert('info', $message);
  }
}
if ( ! function_exists('warning'))
{
    /**
    * warning
    *
    * Create a Bootstrap 4 Alert "Warning" element with a close button
    *
    * @param	string
    * @return	array	depends on what the array contains
    */
  function warning($message = false)
  {
    return createAlert('warning', $message);
  }
}
if ( ! function_exists('danger'))
{
    /**
    * danger
    *
    * Create a Bootstrap 4 Alert "Danger" element with a close button
    *
    * @param	string
    * @return	array	depends on what the array contains
    */
  function danger($message = false)
  {
    return createAlert('danger', $message);
  }
}
if ( ! function_exists('primary'))
{
    /**
    * primary
    *
    * Create a Bootstrap 4 Alert "Primary" element with a close button
    *
    * @param	string
    * @return	array	depends on what the array contains
    */
  function primary($message = false)
  {
    return createAlert('primary', $message);
  }
}
if ( ! function_exists('secondary'))
{
    /**
    * secondary
    *
    * Create a Bootstrap 4 Alert "Secondary" element with a close button
    *
    * @param	string
    * @return	array	depends on what the array contains
    */
  function secondary($message = false)
  {
    return createAlert('secondary', $message);
  }
}
if ( ! function_exists('dark'))
{
    /**
    * dark
    *
    * Create a Bootstrap 4 Alert "Dark" element with a close button
    *
    * @param	string
    * @return	array	depends on what the array contains
    */
  function dark($message = false)
  {
    return createAlert('dark', $message);
  }
}
if ( ! function_exists('light'))
{
    /**
    * light
    *
    * Create a Bootstrap 4 Alert "Light" element with a close button
    *
    * @param	string
    * @return	array	depends on what the array contains
    */
  function light($message = false)
  {
    return createAlert('light', $message);
  }
}